# Contributor: Antoine Fontaine <antoine.fontaine@epfl.ch>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=repo
pkgver=2.36.1
pkgrel=0
pkgdesc="repository management tool built on top of git"
url="https://gerrit.googlesource.com/git-repo"
arch="noarch"
license="Apache-2.0"
options="!check" # no tests upstream
depends="python3 git"
makedepends="py3-setuptools"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/GerritCodeReview/git-repo/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/git-repo-$pkgver"

prepare() {
	default_prepare

	sed -i 's|^#!/usr/bin/env python|#!/usr/bin/python3|' repo
}

package() {
	install -Dm755 repo -t "$pkgdir"/usr/bin
	install -Dm644 man/* -t "$pkgdir"/usr/share/man/man1
}

sha512sums="
36d9fd4628f65b2854ea2f800e20b0dad764b55f85af6cd93bfa3e9f7cda7e9e2b7b6698ec10fc613735e701c797c3c55c6c4a9ea9af385dc5ff6f87774c9395  repo-2.36.1.tar.gz
"
