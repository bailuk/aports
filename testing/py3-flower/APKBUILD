# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>

_pyname=flower
pkgname="py3-$_pyname"
pkgver=2.0.0
pkgrel=0
arch="noarch !s390x" # Missing py3-celery for tests
pkgdesc="Real-time monitor and web admin for Celery distributed task queue"
url="https://pypi.python.org/project/$_pyname"
license="BSD-3-Clause"
depends="
	py3-redis
	py3-humanize
	py3-prometheus-client
	py3-click
	"
makedepends="
	py3-setuptools
	py3-gpep517
	py3-wheel
	py3-installer
	"
checkdepends="
	py3-pytest
	py3-tornado
	py3-celery
	"
source="$pkgname-$pkgver.tar.gz::https://pypi.io/packages/source/${_pyname:0:1}/$_pyname/$_pyname-$pkgver.tar.gz"
builddir="$srcdir"/$_pyname-$pkgver
subpackages="$pkgname-pyc"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest -v
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
e199223c05763abbbc0e03b310ae68f18e2d3046fe41dc1ff2f52943d2ada703fce04ff0523cac883828064607593bc583f439a187f7fe20b8ab883a1e1f2331  py3-flower-2.0.0.tar.gz
"
